Include lib/generator.sh
Include lib/utils.sh

print_config_head() {
	INTERFACE="ens3"
	wg() {
		[ "$1" = "genkey" ] && echo "gLZsIyShD0+Ybv1CuC6On2/HtF0aCNtNt4y4CKd+AF8="
	}
	eval "echo \"$(cat config/config_head.template)\""
	unset -f wg
}

print_machine_config() {
	%text
	#|# ID: 61aef0410aae422398b26330b372da22
	#|# COMMENT: test
	#|# Added on: Fri Aug	5 11:37:09 UTC 2022
	#|[Peer]
	#|PublicKey = HX0RNP5BPBfpsPXoM6VLOMS6G8bTOVzZRg8h8Ot5eWs=
	#|PresharedKey = z8lYAUkyRv+CSbPcMM7K8vMt9ZRK/spjI6BEhKRpNsc=
	#|AllowedIPs = 10.0.0.2/32
	#|AllowedIPs = fd70::2/128
}

Describe 'Test generate_machine_config()'
	setup() {
		TARGET_FILE="$(mktemp)"
		ID="61aef0410aae422398b26330b372da22"
		COMMENT="test"
		CLIENT_PUB_KEY="HX0RNP5BPBfpsPXoM6VLOMS6G8bTOVzZRg8h8Ot5eWs="
		PRESHARED_KEY="z8lYAUkyRv+CSbPcMM7K8vMt9ZRK/spjI6BEhKRpNsc="
		FREE_IPv4="10.0.0.2"
		FREE_IPv6="fd70::2"

		date() {
			echo "Fri Aug	5 11:37:09 UTC 2022"
		}
	}
	cleanup() {
		rm "$TARGET_FILE"
	}
	Before setup
	After cleanup

	It 'calls generate_machine_config()'
		When call generate_machine_config
		The status should be success
		The contents of file "$TARGET_FILE" should equal "$(print_machine_config)"
	End
End

Describe 'Test reconfigure_server()'
	setup() {
		TARGET_DIR="$(mktemp -d)"
		INTERFACE_NAME="wg"
		IPv4_SUBNET="10.0.0.0/24"
		IPv6_SUBNET="fd70::0/120"
		ID="61aef0410aae422398b26330b372da22"
		COMMENT="test"
		CLIENT_PUB_KEY="HX0RNP5BPBfpsPXoM6VLOMS6G8bTOVzZRg8h8Ot5eWs="
		PRESHARED_KEY="z8lYAUkyRv+CSbPcMM7K8vMt9ZRK/spjI6BEhKRpNsc="
		ENDPOINT="server.tld:51820"
		DNS="192.168.10.1"
		DNS_SEARCH="dns.local"

		print_config_head > "$TARGET_DIR/config_head"

		wg() {
			[ "$1" = "pubkey" ] && sed "s#gLZsIyShD0+Ybv1CuC6On2/HtF0aCNtNt4y4CKd+AF8=#0aLkSpBTP6Kadr//Bj3QtRcbaLTlvQUsV8DPPD6rUQ4=#"
		}
	}
	cleanup() {
		rm -r "$TARGET_DIR"
	}
	Before setup
	After cleanup

	It 'calls reconfigure_server()'
		When run reconfigure_server
		The status should be success
		The output should equal '0.2;0aLkSpBTP6Kadr//Bj3QtRcbaLTlvQUsV8DPPD6rUQ4=;10.0.0.2;fd70::2;server.tld:51820;192.168.10.1;dns.local'
	End

	It 'calls reconfigure_server() with existing servers'
		echo "
		fd70::1d
		10.0.0.7
		fd70::2
		10.0.0.2
		" > "$TARGET_DIR/config"
		# fd70::1 to fd70::1b
		for i in $(seq 27 | dec2hex); do
			echo "fd70::$i"
		done >> "$TARGET_DIR/config"
		When call reconfigure_server
		The status should be success
		The output should equal '0.2;0aLkSpBTP6Kadr//Bj3QtRcbaLTlvQUsV8DPPD6rUQ4=;10.0.0.3;fd70::1c;server.tld:51820;192.168.10.1;dns.local'
	End
End
