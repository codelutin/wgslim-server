#!/bin/bash

set -e

DIR="$(dirname "$(realpath "$0")")"

source "$DIR/lib/utils.sh"
source "$DIR/lib/generator.sh"

source "$DIR/config/wgslim.conf.sh" || err "config file not found"

[ $# -ne 4 ] && echo "Usage: $0 <ID> <COMMENT> <CLIENT_PUB_KEY> <PRESHARED_KEY>" && exit 0

ID="$1"
COMMENT="$2"
CLIENT_PUB_KEY="$3"
PRESHARED_KEY="$4"

TARGET_DIR="$WGSLIM_CONFIG_DIR/$INTERFACE_NAME"

reconfigure_server

# Reload server config without downtime ('wg sysconf' doesn't create routes,
# this should be handled in $INTERFACE_NAME.conf, for example by using a /24 as Adress)
wg syncconf "$INTERFACE_NAME" <(wg-quick strip "$TARGET_DIR/config")
