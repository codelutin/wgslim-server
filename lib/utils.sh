# err <msg>
err() {
	echo -e "\033[1;31mError (server): $1\033[0m"
	exit 1
}

# testerr <msg>
# For use in tests only
testerr() {
	echo -e "\033[1;31mError (server): $1\033[0m"
}

# warn <msg>
warn() {
	echo -e "\033[1;33mWarning (server): $1\033[0m"
}

# info <msg>
info() {
	echo -e "\033[1;32mInfo (server): $1\033[0m"
}

hex2dec() {
	while read num; do
		echo "$((0x$num))"
	done
}

dec2hex() {
	while read num; do
		printf '%x\n' $num
	done
}

# get_ipv4_suffixes <FILE> <IPv4_PREFIX>
get_ipv4_suffixes() {
	local FILE="$1"
	local IPv4_PREFIX="$2"

	grep -oE "$IPv4_PREFIX[0-9]{1,3}" "$FILE" 2>/dev/null | sed "s/$IPv4_PREFIX//" | sort -un
	#bash: grep -oP "(?<=$2)[0-9]{1,3}" "$1" 2>/dev/null | sort -un
}

# get_ipv4_suffixes <FILE<IPv6_PREFIX>
get_ipv6_suffixes() {
	local FILE="$1"
	local IPv6_PREFIX="$2"

	grep -oE "$IPv6_PREFIX[a-fA-F0-9]{1,4}" "$FILE" 2>/dev/null | sed "s/$IPv6_PREFIX//; " | hex2dec | sort -un
	#bash: grep -oP "(?<=$2)[0-9]{1,3}" "$1" 2>/dev/null | sort -un
}

# (WIP) find_free_suffix
# stdin: suffixes (one per line)
# stdout: free suffix
find_free_suffix() {
	local PREV=1 RESULT
	while read CURRENT; do
		if [ $((CURRENT - PREV)) -gt 1 ]; then
			RESULT="$((PREV+1))"
			break
		fi
		PREV="$CURRENT"
	done

	[ -z "${RESULT}" ] && RESULT="$((PREV+1))"
	[ "$RESULT" -gt 255 ] && err "Can not find free IP"

	echo "$RESULT"
}

# find_free_ipv4
# stdout: free ipv4
find_free_ipv4() {
	needs TARGET_DIR IPv4_SUBNET
	[ $(echo "$IPv4_SUBNET" | cut -d '/' -f2) -ne 24 ] && err "only /24 subnets are supported for the moment"
	local IPv4_PREFIX="$(echo "$IPv4_SUBNET" | cut -d '/' -f1 | sed -E "s/[0-9]{1,3}$//")"
	local FREE_SUFFIX_v4=$(get_ipv4_suffixes "$TARGET_DIR/config" "$IPv4_PREFIX" | find_free_suffix)
	echo "$IPv4_PREFIX$FREE_SUFFIX_v4"
}

# find_free_ipv6
# stdout: free ipv6
find_free_ipv6() {
	needs TARGET_DIR IPv6_SUBNET
	[ $(echo "$IPv6_SUBNET" | cut -d '/' -f2) -ne 120 ] && err "only /120 subnets are supported for the moment"
	local IPv6_PREFIX="$(echo "$IPv6_SUBNET" | cut -d '/' -f1 | sed -E "s/[a-fA-F0-9]{1,4}$//")"
	local FREE_SUFFIX_v6=$(get_ipv6_suffixes "$TARGET_DIR/config" "$IPv6_PREFIX" | find_free_suffix | dec2hex)
	echo "$IPv6_PREFIX$FREE_SUFFIX_v6"
}

# Utils from wgslim-client (tested there)

# needs <var_1> ... <var_n>
needs() {
	local var_name
	for var_name in "$@"; do
		if expr match "$var_name" "[a-zA-Z0-9_]\+$" > /dev/null; then
			local var_contents="$(eval "echo \${$var_name}")"
			[ -z "$var_contents" ] && err "var \$$var_name is empty"
		else
			err "Invalid var name: $var_name"
		fi
	done
	return 0
}
