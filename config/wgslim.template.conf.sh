ENDPOINT="" # Required
DNS=""
DNS_SEARCH=""
INTERFACE_NAME="wg"
IPv4_SUBNET="10.0.0.0/24"
IPv6_SUBNET="fd70::0/120"
WGSLIM_CONFIG_DIR="/usr/local/etc/wgslim" # Expected to be writable by the user running wgslim
