# generate_machine_config
generate_machine_config() {
	needs TARGET_FILE ID COMMENT CLIENT_PUB_KEY PRESHARED_KEY FREE_IPv4 FREE_IPv6

	cat <<- EOF > "$TARGET_FILE"
	# ID: $ID
	# COMMENT: $COMMENT
	# Added on: $(date)
	[Peer]
	PublicKey = $CLIENT_PUB_KEY
	PresharedKey = $PRESHARED_KEY
	AllowedIPs = $FREE_IPv4/32
	AllowedIPs = $FREE_IPv6/128
	EOF
}

# reconfigure_server
# returns: <RESPONSE_VERSION> <SERVER_PUB_KEY> <FREE_IPv4> <FREE_IPv6> <ENDPOINT> <DNS> <DNS_SEARCH>
reconfigure_server() {
	needs TARGET_DIR IPv4_SUBNET IPv6_SUBNET ID COMMENT CLIENT_PUB_KEY PRESHARED_KEY ENDPOINT DNS DNS_SEARCH

	mkdir -p "$TARGET_DIR/machines"

	local TARGET_FILE="$TARGET_DIR/machines/$ID"
	local CONFIG_HEAD_FILE="$TARGET_DIR/config_head"
	[ -f "$TARGET_FILE" ] && err "keys for this machine already exist"
	[ ! -f "$CONFIG_HEAD_FILE" ] && err "file $CONFIG_HEAD_FILE does not exist (or not a regular file)"

	# Calculate the server public key
	local SERVER_PUB_KEY=$(grep PrivateKey "$CONFIG_HEAD_FILE" | sed -E 's/PrivateKey\s*=\s*//' | wg pubkey)

	# Find available IPs
	local FREE_IPv4="$(find_free_ipv4)"
	local FREE_IPv6="$(find_free_ipv6)"

	generate_machine_config

	# (Re)generate server config
	cat "$TARGET_DIR/config_head" "$TARGET_DIR/machines/"* > "$TARGET_DIR/config"

	# Print the necessary values for the client
	echo "0.2;$SERVER_PUB_KEY;$FREE_IPv4;$FREE_IPv6;$ENDPOINT;$DNS;$DNS_SEARCH"
}
