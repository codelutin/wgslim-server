Include lib/utils.sh

Describe 'Test find_free_suffix()'
	# Prepare test data
	DATA_FILE_EMPTY=$(mktemp)
	DATA_FILE_FULL=$(mktemp) # 1-255 taken
	DATA_FILE_3=$(mktemp) # 1-200, 202-255 taken
	DATA_FILE_4=$(mktemp) # 1-200, 210 taken
	DATA_FILE_5=$(mktemp) # 1-200 taken

	IPv4_PREFIX="10.0.0."
	IPv6_PREFIX="fd70::"

	# populate <file> <n1> <n2>
	populate() {
		for i in $(seq $2 $3); do
			echo ${IPv4_PREFIX}$i >> $1
		done
	}

	populate $DATA_FILE_FULL 1 255
	populate $DATA_FILE_3 1 200
	populate $DATA_FILE_3 202 255
	populate $DATA_FILE_4 1 200
	populate $DATA_FILE_4 210 210
	populate $DATA_FILE_5 1 200

	It 'calls find_free_suffix() with empty file'
		Data get_ipv4_suffixes $DATA_FILE_EMPTY $IPv4_PREFIX
		When call find_free_suffix
		The output should equal "2"
	End

	It 'calls find_free_suffix() with full file'
		Data get_ipv4_suffixes $DATA_FILE_FULL $IPv4_PREFIX
		When run find_free_suffix
		The status should be failure
		The output should equal "$(testerr 'Can not find free IP')"
	End

	It 'calls find_free_suffix() with file 3'
		Data get_ipv4_suffixes $DATA_FILE_3 $IPv4_PREFIX
		When call find_free_suffix
		The output should equal "201"
	End
	It 'calls find_free_suffix() with file 4'
		Data get_ipv4_suffixes $DATA_FILE_4 $IPv4_PREFIX
		When call find_free_suffix
		The output should equal "201"
	End
	It 'calls find_free_suffix() with file 5'
		Data get_ipv4_suffixes $DATA_FILE_5 $IPv4_PREFIX
		When call find_free_suffix
		The output should equal "201"
	End

	rm "$DATA_FILE_EMPTY" "$DATA_FILE_FULL" "$DATA_FILE_3" "$DATA_FILE_4" "$DATA_FILE_5"
End
